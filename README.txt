HOWTO - Start

# Requirements
   JDK 1.8
   Maven 3
   git
   
# Checkout
   $ git clone https://emin_karayel@bitbucket.org/emin_karayel/poker-hands.git

# Application
   Requires permission to listen on localhost:8080
   $ mvn compile exec:java
   and open the URL with your favorite Web-Browser: http://127.0.0.1:8080

# Unit-Tests (with mutations tests)
   $ mvn org.pitest:pitest-maven:mutationCoverage
