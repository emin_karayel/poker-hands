package org.bitbucket.emin_karayel.pokerhands;

import org.bitbucket.emin_karayel.pokerhands.Hand.ComparisonResult;
import org.junit.Test;

import static org.junit.Assert.*; 

public class SystematicRuleTest {
    
    private final Hand illegalHand = Hand.createHand(new Card[] {
        Card.createCard(Card.Rank.ACE, Card.Suite.HEARTS),
        Card.createCard(Card.Rank.KING, Card.Suite.HEARTS),
        Card.createCard(Card.Rank.QUEEN, Card.Suite.HEARTS),
        Card.createCard(Card.Rank.TEN, Card.Suite.HEARTS),
        Card.createCard(Card.Rank.TEN, Card.Suite.HEARTS)
    });
            
    public static final Hand[] orderedHands = new Hand[] {
        Hand.createHand(new Card[] {
            Card.createCard(Card.Rank.ACE, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.KING, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.QUEEN, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.JACK, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.TEN, Card.Suite.HEARTS)
        }),
        Hand.createHand(new Card[] {
            Card.createCard(Card.Rank.KING, Card.Suite.SPADES),
            Card.createCard(Card.Rank.QUEEN, Card.Suite.SPADES),
            Card.createCard(Card.Rank.JACK, Card.Suite.SPADES),
            Card.createCard(Card.Rank.TEN, Card.Suite.SPADES),
            Card.createCard(Card.Rank.NINE, Card.Suite.SPADES)
        }),
        Hand.createHand(new Card[] {
            Card.createCard(Card.Rank.NINE, Card.Suite.SPADES),
            Card.createCard(Card.Rank.EIGHT, Card.Suite.SPADES),
            Card.createCard(Card.Rank.SEVEN, Card.Suite.SPADES),
            Card.createCard(Card.Rank.SIX, Card.Suite.SPADES),
            Card.createCard(Card.Rank.FIVE, Card.Suite.SPADES)
        }),
        Hand.createHand(new Card[] {
            Card.createCard(Card.Rank.JACK, Card.Suite.SPADES),
            Card.createCard(Card.Rank.JACK, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.JACK, Card.Suite.DIAMONDS),
            Card.createCard(Card.Rank.JACK, Card.Suite.CLUBS),
            Card.createCard(Card.Rank.SEVEN, Card.Suite.SPADES)
        }),
        Hand.createHand(new Card[] {
            Card.createCard(Card.Rank.EIGHT, Card.Suite.SPADES),
            Card.createCard(Card.Rank.EIGHT, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.EIGHT, Card.Suite.DIAMONDS),
            Card.createCard(Card.Rank.EIGHT, Card.Suite.CLUBS),
            Card.createCard(Card.Rank.KING, Card.Suite.SPADES)
        }),        
        Hand.createHand(new Card[] {
                Card.createCard(Card.Rank.ACE, Card.Suite.SPADES),
                Card.createCard(Card.Rank.ACE, Card.Suite.HEARTS),
                Card.createCard(Card.Rank.ACE, Card.Suite.DIAMONDS),
                Card.createCard(Card.Rank.NINE, Card.Suite.CLUBS),
                Card.createCard(Card.Rank.NINE, Card.Suite.SPADES)
            }),
        Hand.createHand(new Card[] {
            Card.createCard(Card.Rank.NINE, Card.Suite.SPADES),
            Card.createCard(Card.Rank.NINE, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.NINE, Card.Suite.DIAMONDS),
            Card.createCard(Card.Rank.ACE, Card.Suite.CLUBS),
            Card.createCard(Card.Rank.ACE, Card.Suite.SPADES)
        }),
        Hand.createHand(new Card[] {
            Card.createCard(Card.Rank.SEVEN, Card.Suite.SPADES),
            Card.createCard(Card.Rank.THREE, Card.Suite.SPADES),
            Card.createCard(Card.Rank.NINE, Card.Suite.SPADES),
            Card.createCard(Card.Rank.KING, Card.Suite.SPADES),
            Card.createCard(Card.Rank.FIVE, Card.Suite.SPADES)
        }),
        Hand.createHand(new Card[] {
            Card.createCard(Card.Rank.FOUR, Card.Suite.DIAMONDS),
            Card.createCard(Card.Rank.FIVE, Card.Suite.SPADES),
            Card.createCard(Card.Rank.SIX, Card.Suite.CLUBS),
            Card.createCard(Card.Rank.SEVEN, Card.Suite.SPADES),
            Card.createCard(Card.Rank.EIGHT, Card.Suite.DIAMONDS)
        }),
        Hand.createHand(new Card[] {
            Card.createCard(Card.Rank.SIX, Card.Suite.DIAMONDS),
            Card.createCard(Card.Rank.TWO, Card.Suite.SPADES),
            Card.createCard(Card.Rank.THREE, Card.Suite.CLUBS),
            Card.createCard(Card.Rank.FOUR, Card.Suite.SPADES),
            Card.createCard(Card.Rank.FIVE, Card.Suite.DIAMONDS)
        }),
        Hand.createHand(new Card[] {
                Card.createCard(Card.Rank.ACE, Card.Suite.DIAMONDS),
                Card.createCard(Card.Rank.TWO, Card.Suite.SPADES),
                Card.createCard(Card.Rank.THREE, Card.Suite.CLUBS),
                Card.createCard(Card.Rank.FOUR, Card.Suite.SPADES),
                Card.createCard(Card.Rank.FIVE, Card.Suite.DIAMONDS)
        }),
        Hand.createHand(new Card[] {
            Card.createCard(Card.Rank.SEVEN, Card.Suite.DIAMONDS),
            Card.createCard(Card.Rank.SEVEN, Card.Suite.SPADES),
            Card.createCard(Card.Rank.SEVEN, Card.Suite.CLUBS),
            Card.createCard(Card.Rank.TEN, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.EIGHT, Card.Suite.DIAMONDS)
        }),
        Hand.createHand(new Card[] {
            Card.createCard(Card.Rank.QUEEN, Card.Suite.DIAMONDS),
            Card.createCard(Card.Rank.QUEEN, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.SIX, Card.Suite.CLUBS),
            Card.createCard(Card.Rank.SIX, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.JACK, Card.Suite.DIAMONDS)
        }),
        Hand.createHand(new Card[] {
            Card.createCard(Card.Rank.FOUR, Card.Suite.DIAMONDS),
            Card.createCard(Card.Rank.FOUR, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.TWO, Card.Suite.CLUBS),
            Card.createCard(Card.Rank.SEVEN, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.ACE, Card.Suite.DIAMONDS)
        }),
        Hand.createHand(new Card[] {
            Card.createCard(Card.Rank.QUEEN, Card.Suite.DIAMONDS),
            Card.createCard(Card.Rank.FOUR, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.TWO, Card.Suite.CLUBS),
            Card.createCard(Card.Rank.SEVEN, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.ACE, Card.Suite.DIAMONDS)
        }),
        Hand.createHand(new Card[] {
            Card.createCard(Card.Rank.QUEEN, Card.Suite.DIAMONDS),
            Card.createCard(Card.Rank.FOUR, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.TWO, Card.Suite.CLUBS),
            Card.createCard(Card.Rank.THREE, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.FIVE, Card.Suite.DIAMONDS)
        }),
        Hand.createHand(new Card[] {
            Card.createCard(Card.Rank.JACK, Card.Suite.DIAMONDS),
            Card.createCard(Card.Rank.FOUR, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.NINE, Card.Suite.CLUBS),
            Card.createCard(Card.Rank.SEVEN, Card.Suite.HEARTS),
            Card.createCard(Card.Rank.EIGHT, Card.Suite.DIAMONDS)
        }),
    };
    
    @Test
    public void testSystematic()
    {
        for (int i = 0; i < orderedHands.length; i ++) {
            for (int j = 0; j < orderedHands.length; j ++) {
                ComparisonResult result = CompareHands.compare(orderedHands[i], orderedHands[j]);
                if ( i < j ) {
                    assertEquals(ComparisonResult.WIN, result);
                } else if( i > j ) {
                    assertEquals(ComparisonResult.LOOSE, result);
                } else {
                    assertEquals(ComparisonResult.DRAW, result);
                }
                
            }            
        }
    }

    @Test
    public void testIllegal()
    {
        for(Hand o: orderedHands) {
            assertEquals(ComparisonResult.ILLEGAL, CompareHands.compare(o, illegalHand));
            assertEquals(ComparisonResult.ILLEGAL, CompareHands.compare(illegalHand, o));
        }
    }
    
}
