package org.bitbucket.emin_karayel.pokerhands;

import org.junit.Test;
import static org.junit.Assert.*;

import org.bitbucket.emin_karayel.pokerhands.Hand.ComparisonResult;

public class RuleTest {

    @Test
    public void drawFourOfAKind() {
        assertEquals(ComparisonResult.DRAW, CompareHands.compare(
            Hand.createHand(new Card[]{
                    Card.createCard(Card.Rank.THREE, Card.Suite.DIAMONDS),
                    Card.createCard(Card.Rank.THREE, Card.Suite.SPADES),
                    Card.createCard(Card.Rank.THREE, Card.Suite.HEARTS),
                    Card.createCard(Card.Rank.THREE, Card.Suite.CLUBS),
                    Card.createCard(Card.Rank.JACK, Card.Suite.DIAMONDS),
            }), Hand.createHand(new Card[]{
                    Card.createCard(Card.Rank.THREE, Card.Suite.HEARTS),
                    Card.createCard(Card.Rank.THREE, Card.Suite.CLUBS),
                    Card.createCard(Card.Rank.THREE, Card.Suite.DIAMONDS),
                    Card.createCard(Card.Rank.THREE, Card.Suite.SPADES),
                    Card.createCard(Card.Rank.TEN, Card.Suite.CLUBS),
            })
        ));
    }    
}
