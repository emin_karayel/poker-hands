package org.bitbucket.emin_karayel.pokerhands;

import static org.junit.Assert.*;
import org.junit.Test;

public class QueryParamsTest  {
    @Test
    public void testLegal() {
        assertEquals(
            Hand.fromQueryParam("0,0,1,0,2,3,4,1,5,2"),
            Hand.createHand(new Card[] {
                new Card(Card.Rank.TWO, Card.Suite.HEARTS),
                new Card(Card.Rank.THREE, Card.Suite.HEARTS),
                new Card(Card.Rank.FOUR, Card.Suite.SPADES),
                new Card(Card.Rank.SIX, Card.Suite.DIAMONDS),
                new Card(Card.Rank.SEVEN, Card.Suite.CLUBS)
            })
        );
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testIllegalLength() {
        Hand.fromQueryParam("1,2");
    };

    @Test(expected=ArrayIndexOutOfBoundsException.class)
    public void testIllegalRank() {
        Hand.fromQueryParam("0,0,32,0,2,3,4,1,5,2");
    };

    @Test(expected=ArrayIndexOutOfBoundsException.class)
    public void testIllegalSuite() {
        Hand.fromQueryParam("0,0,1,99,2,3,4,1,5,2");
    };

}
