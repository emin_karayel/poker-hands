package org.bitbucket.emin_karayel.pokerhands;

import static org.junit.Assert.*;

import org.junit.Test;

public class HandTest {

    @Test
    public void legalHandTest() {
        Hand h = Hand.createHand(new Card[]{
                Card.createCard(Card.Rank.ACE, Card.Suite.CLUBS),
                Card.createCard(Card.Rank.TWO, Card.Suite.DIAMONDS),
                Card.createCard(Card.Rank.TWO, Card.Suite.HEARTS),
                Card.createCard(Card.Rank.THREE, Card.Suite.DIAMONDS),
                Card.createCard(Card.Rank.QUEEN, Card.Suite.DIAMONDS)
        });
        assertTrue(h.isLegalHand());
    }

    @Test
    public void illegalHandTest() {
        Hand h = Hand.createHand(new Card[]{
                Card.createCard(Card.Rank.THREE, Card.Suite.DIAMONDS),
                Card.createCard(Card.Rank.TWO, Card.Suite.DIAMONDS),
                Card.createCard(Card.Rank.TWO, Card.Suite.HEARTS),
                Card.createCard(Card.Rank.THREE, Card.Suite.DIAMONDS),
                Card.createCard(Card.Rank.QUEEN, Card.Suite.DIAMONDS)
        });
        assertFalse(h.isLegalHand());
    }

    @Test(expected=IllegalArgumentException.class)
    public void tooFewCardsTest() {
        Hand.createHand(new Card[]{
                Card.createCard(Card.Rank.THREE, Card.Suite.DIAMONDS),
                Card.createCard(Card.Rank.TWO, Card.Suite.DIAMONDS),
                Card.createCard(Card.Rank.TWO, Card.Suite.HEARTS),
                Card.createCard(Card.Rank.THREE, Card.Suite.DIAMONDS),
        });
    }

    @Test(expected=IllegalArgumentException.class)
    public void nullCardTest() {
        Hand.createHand(new Card[]{
                Card.createCard(Card.Rank.THREE, Card.Suite.DIAMONDS),
                Card.createCard(Card.Rank.TWO, Card.Suite.DIAMONDS),
                Card.createCard(Card.Rank.TWO, Card.Suite.HEARTS),
                Card.createCard(Card.Rank.THREE, Card.Suite.DIAMONDS),
                null
        });
    }
}
