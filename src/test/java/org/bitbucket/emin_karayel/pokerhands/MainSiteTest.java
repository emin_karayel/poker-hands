package org.bitbucket.emin_karayel.pokerhands;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.junit.Test;

import freemarker.template.TemplateException;
import spark.RequestResponseFactory;

public class MainSiteTest {
    @Test
    public void smokeTest() throws IOException, TemplateException {
        MainSite mainsite = new MainSite("main-site-mock.ftl");
        
        HttpServletResponse response = mock(HttpServletResponse.class);    
        String body = mainsite.handle(RequestResponseFactory.create(response));
        verify(response).setContentType("text/html");
        assertEquals(Integer.toString(Card.Suite.values().length), body);
    }

}
