package org.bitbucket.emin_karayel.pokerhands;

import org.junit.Test;

import freemarker.template.TemplateException;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashMap;

public class FreeMarkerTest {
    @Test
    public void helloWorldTest() throws IOException, TemplateException {
        assertEquals(
            "Hello world.",
            new FreeMarker().renderTemplate("test.ftl", new HashMap<>())
        );
    }    
}
