package org.bitbucket.emin_karayel.pokerhands;

import java.io.IOException;
import java.util.HashMap;
import static org.junit.Assert.*;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.*;
import org.junit.Test;

import freemarker.template.TemplateException;
import spark.RequestResponseFactory;

public class CompareHandsApiTest {

    @Test
    public void smokeRequest() throws IOException, TemplateException
    {
        CompareHandsApi compareHandsApi = new CompareHandsApi();
        HashMap<String, String[]> request = new HashMap<>();
        request.put("a", new String[]{"0,0,1,0,2,3,4,1,5,2"});
        request.put("b", new String[]{"0,0,1,0,2,3,4,1,6,2"});
        request.put("callback", new String[]{"foo"});
        
        HttpServletResponse response = mock(HttpServletResponse.class);    
        String body = compareHandsApi.handle(request, RequestResponseFactory.create(response));
        verify(response).setContentType("text/javascript");
        assertTrue( body.contains("LOOSE"));
        assertFalse( body.contains("WIN"));
    }
}
