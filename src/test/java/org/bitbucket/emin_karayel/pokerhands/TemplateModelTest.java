package org.bitbucket.emin_karayel.pokerhands;

import org.bitbucket.emin_karayel.pokerhands.Card.Suite;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List; 

public class TemplateModelTest {
    @SuppressWarnings("unchecked")
    @Test
    public void testTemplateModel()
    {
        HashMap<String, Object> model = TemplateModel.getTemplateModel();
        
        assertEquals(model.get("handsize"),Hand.HAND_SIZE);
        assertEquals(
            ((List<String>)model.get("suites")).get(1),
            Card.Suite.values()[1].getResourceName()
        );
        assertEquals(
            ((List<String>)model.get("ranks")).get(5),
            Card.Rank.values()[5].getResourcePrefix()
        );
        assertEquals(
            ((List<String>)model.get("ranksuffixes")).get(8),
            Card.Rank.values()[8].getResourceSuffix()
        );
    }
}
