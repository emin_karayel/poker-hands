package org.bitbucket.emin_karayel.pokerhands;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

import static org.junit.Assert.*;

public class ResourceNameTest {
    
    @Test
    public void pairAgainstHighCard() throws IOException {
        for (Card.Rank r : Card.Rank.values()) {
            for (Card.Suite s : Card.Suite.values()) {
                String resName = String.format(
                        "/images/%s_of_%s.png",
                        r.getResourcePrefix(),
                        s.getResourceName(),
                        r.getResourceSuffix()
                );
                assertNotNull(r.getResourceSuffix());
                
                InputStream str = ResourceNameTest.class.getResourceAsStream(resName);
                assertNotNull(resName, str);
                str.close();
            }
        }
        
    }
}
