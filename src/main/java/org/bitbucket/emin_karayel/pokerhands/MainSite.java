package org.bitbucket.emin_karayel.pokerhands;

import java.io.IOException;
import java.util.HashMap;

import freemarker.template.TemplateException;
import spark.Response;

/**
 * Handles requests for the main site
 */
public class MainSite {
    
    private final String templateName;
    
    public MainSite(String templateName)
    {
        this.templateName = templateName;
    }
    
    private final FreeMarker freeMarker = new FreeMarker();
    private final HashMap<String, Object> templateModel = TemplateModel.getTemplateModel();
    
    public String handle(Response response) throws IOException, TemplateException {
        response.type("text/html");
        return freeMarker.renderTemplate(templateName, templateModel);        
    }
}
