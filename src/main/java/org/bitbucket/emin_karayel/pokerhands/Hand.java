package org.bitbucket.emin_karayel.pokerhands;

import java.util.Arrays;

/**
 * Poker hand with 5 cards
 */
public final class Hand {
    
    public enum ComparisonResult {
        WIN,
        LOOSE,
        DRAW,
        ILLEGAL
    }
    public static final int HAND_SIZE = 5;
    
    private final Card[] cards;
    private Hand(Card[] cards)
    {
        this.cards = cards;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(cards);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Hand other = (Hand) obj;
        if (!Arrays.equals(cards, other.cards))
            return false;
        return true;
    }

    public Card getCard(int index)
    {
        return cards[index];
    }
    
    /**
     * Return false iff the hand contains a card twice 
     * @return
     */
    public boolean isLegalHand()
    {
        for (int i = 0; i < HAND_SIZE-1; i++) {
            for (int j = i+1; j < HAND_SIZE; j++) {
                if ( getCard(i).equals(getCard(j))) return false;
            }
        }
        return true;
    }
    
    /**
     * Parses a Hand from a URL query paramter (containing the ordinal values of the Rank and Suite)
     * of its cards.
     * @param param String with 10 comma seperated integers
     * @return
     */
    public static Hand fromQueryParam(String param)
    {
        Card[] cards = new Card[5];
        String[] nums = param.split(",");
        if (nums.length != 2 * HAND_SIZE) throw new IllegalArgumentException("param");
        for (int i = 0; i < HAND_SIZE;i++) {
            Card.Rank r = Card.Rank.values()[Integer.parseInt(nums[2*i])];
            Card.Suite s = Card.Suite.values()[Integer.parseInt(nums[2*i+1])];
            cards[i] = Card.createCard(r, s);
        }
        return createHand(cards);
    }
    
    public static Hand createHand(Card[] cards)
    {
        if (cards.length != HAND_SIZE) throw new IllegalArgumentException();
        for(Card c: cards) if (c == null ) throw new IllegalArgumentException();
        return new Hand(cards);
    }
}
