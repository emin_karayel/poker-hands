package org.bitbucket.emin_karayel.pokerhands;

import java.util.Arrays;
import java.util.List;

import org.bitbucket.emin_karayel.pokerhands.Hand.ComparisonResult;
import org.bitbucket.emin_karayel.pokerhands.partialorders.*;

public abstract class CompareHands {
    
    private static List<PokerHandClass> getPartialOrders() {
        return Arrays.asList(
            StraightFlush.getInstance(),
            FourOfAKind.getInstance(),
            FullHouse.getInstance(),
            Flush.getInstance(),
            Straight.getInstance(),
            ThreeOfAKind.getInstance(),
            TwoPair.getInstance(),
            Pair.getInstance(),
            HighCard.getInstance()
        );
    }
    
    /**
     * Compare two hands according to the rules in e.g. 
     * https://en.wikipedia.org/wiki/List_of_poker_hands.
     * 
     * Will return ComparionsonResult.ILLEGAL if either of the hands contains a duplicate.
     * @param h1
     * @param h2
     * @return
     */
    public static ComparisonResult compare(Hand h1, Hand h2)
    {
        if (!h1.isLegalHand() || !h2.isLegalHand()) return ComparisonResult.ILLEGAL;
        
        for (PokerHandClass o : getPartialOrders()) {
            List<Integer> thisInClass = o.belongsToClass(h1);
            List<Integer> otherInClass = o.belongsToClass(h2);
            if (thisInClass != null && otherInClass != null) {
                int i = 0;
                while (i < thisInClass.size()) {
                    int c = thisInClass.get(i).compareTo(otherInClass.get(i));
                    if ( c > 0 ) return ComparisonResult.WIN;
                    if ( c < 0 ) return ComparisonResult.LOOSE;
                    i++;
                }
                return ComparisonResult.DRAW;
            }
            if (thisInClass != null) return ComparisonResult.WIN;
            if (otherInClass != null) return ComparisonResult.LOOSE;
        }
        
        throw new IllegalStateException("Error in partial orders.");
    }
}
