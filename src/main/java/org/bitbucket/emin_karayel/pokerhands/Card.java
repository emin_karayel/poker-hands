package org.bitbucket.emin_karayel.pokerhands;

/**
 * Card of a poker hand
 */
public final class Card {
    
    public enum Suite {
        HEARTS("hearts"),
        DIAMONDS("diamonds"),
        CLUBS("clubs"),
        SPADES("spades");
        
        private final String resourceName;
        
        Suite(String resourceName) {
            this.resourceName = resourceName;
        }

        /**
         * @return Part of the image file name denoting the suite
         */
        public String getResourceName() {
            return resourceName;
        }
    }
    
    public enum Rank {
        TWO("2",""),
        THREE("3",""),
        FOUR("4",""),
        FIVE("5",""),
        SIX("6",""),
        SEVEN("7",""),
        EIGHT("8",""),
        NINE("9",""),
        TEN("10",""),
        JACK("jack","2"),
        QUEEN("queen","2"),
        KING("king","2"),
        ACE("ace","");
        
        private final String resourcePrefix;
        private final String resourceSuffix;
        
        Rank(String prefix, String suffix) {
            this.resourcePrefix = prefix;
            this.resourceSuffix = suffix;
        }

        /**
         * @return Prefix of all image-filenames for this rank
         */
        public String getResourcePrefix() {
            return resourcePrefix;
        }
        
        /**
         * @return Suffix of all image-filenames for this rank
         */
        public String getResourceSuffix() {
            return resourceSuffix;
        }
    }
    
    public Card(Rank rank, Suite suite)
    {
        this.rank = rank;
        this.suite = suite;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + rank.ordinal();
        result = prime * result + suite.ordinal();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Card other = (Card) obj;
        if (rank != other.rank)
            return false;
        if (suite != other.suite)
            return false;
        return true;
    }

    public static Card createCard(Rank rank, Suite suite)
    {
        return new Card(rank, suite);
    }
    
    private final Rank rank;
    private final Suite suite;
    
    public Rank getRank()
    {
        return rank;
    }
    
    public Suite getSuite()
    {
        return suite;
    }
}
