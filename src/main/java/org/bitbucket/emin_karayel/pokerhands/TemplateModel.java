package org.bitbucket.emin_karayel.pokerhands;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Returns data model required for rendering the main-site using freemarker
 */
public abstract class TemplateModel {
    
    public static HashMap<String,Object> getTemplateModel()
    {
        HashMap<String, Object> result = new HashMap<>();
        result.put("ranks", mapList(Card.Rank.values(), (r)->r.getResourcePrefix()));
        result.put("ranksuffixes", mapList(Card.Rank.values(), (r)->r.getResourceSuffix()));
        result.put("handsize", Hand.HAND_SIZE);
        result.put("suites", mapList(Card.Suite.values(), (r)->r.getResourceName()));
        return result;
    }

    private static <T> List<String> mapList(T[] arr, Function<T,String> fun)
    {
        return Arrays.asList(arr).stream().map(fun).collect(Collectors.toList());
    }
    
}
