package org.bitbucket.emin_karayel.pokerhands;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import freemarker.template.TemplateException;
import spark.Response;

/**
 * Hands requests for the comparehands api using CompareHands.compare 
 */
public class CompareHandsApi {

    private final FreeMarker freeMarker = new FreeMarker();

    public String handle(Map<String, String[]> request, Response response) throws IOException, TemplateException {
        response.type("text/javascript");
        Hand.ComparisonResult result = CompareHands.compare(
            Hand.fromQueryParam(request.get("a")[0]),
            Hand.fromQueryParam(request.get("b")[0])
        );
        HashMap<String, Object> model = new HashMap<>();
        model.put("callback",request.get("callback")[0]);
        model.put("comparisonResult",result.toString());
        return freeMarker.renderTemplate("compare-hands-response.ftl", model);
    }
}
