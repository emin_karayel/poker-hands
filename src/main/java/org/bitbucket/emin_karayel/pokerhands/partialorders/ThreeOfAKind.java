package org.bitbucket.emin_karayel.pokerhands.partialorders;

import java.util.Arrays;

public final class ThreeOfAKind extends RankDistributionClass {
    
    private static final ThreeOfAKind instance = new ThreeOfAKind();

    public static ThreeOfAKind getInstance()
    {
        return instance;
    }
    
    private ThreeOfAKind() {
        super(Arrays.asList(1,1,3), true);
    }
}
