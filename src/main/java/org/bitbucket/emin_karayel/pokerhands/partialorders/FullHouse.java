package org.bitbucket.emin_karayel.pokerhands.partialorders;

import java.util.Arrays;
import java.util.stream.Stream;

public final class FullHouse extends RankDistributionClass
{
    private static final FullHouse instance = new FullHouse();

    public static FullHouse getInstance()
    {
        return instance;
    }
    
    private FullHouse() {
        super(Arrays.asList(2,3), true);
    }
}
