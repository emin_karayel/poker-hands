package org.bitbucket.emin_karayel.pokerhands.partialorders;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.bitbucket.emin_karayel.pokerhands.Card;
import org.bitbucket.emin_karayel.pokerhands.Hand;
import org.bitbucket.emin_karayel.pokerhands.Card.Rank;

public final class Straight implements PokerHandClass {

    private static final Straight instance = new Straight();
    
    private Straight()
    {
        
    }
    
    public static Straight getInstance()
    {
        return instance;
    }
    
    @Override
    public List<Integer> belongsToClass(Hand hand) {
        // TODO Auto-generated method stub
        List<Card.Rank> ranks = new ArrayList<>();
        for (int i = 0; i < Hand.HAND_SIZE; i++) {
            ranks.add(hand.getCard(i).getRank());
        }
        ranks.sort(Comparator.naturalOrder());
        
        // 2 3 4 5 A?
        int firstOrdinal = ranks.get(0).ordinal(); 
        for (int i = 1; i < Hand.HAND_SIZE; i++) {
            if (ranks.get(i).ordinal()!=firstOrdinal+i) {
                if (i < Hand.HAND_SIZE -1) return null;
                if (firstOrdinal != 0 || ranks.get(i) != Card.Rank.ACE) return null;
                return Collections.singletonList(ranks.get(Hand.HAND_SIZE-2).ordinal());
            }
        };
        return Collections.singletonList(ranks.get(Hand.HAND_SIZE-1).ordinal());
    }
    

}
