package org.bitbucket.emin_karayel.pokerhands.partialorders;

import java.util.List;

import org.bitbucket.emin_karayel.pokerhands.Hand;

public interface PokerHandClass {
	
	/**
	 * Return not-null if the hand belongs to this class (or possibly a higher class) the resulting
	 * list contains the ranks of the cards with which ties can be broken.
	 * 
	 * @param hand
	 * @return
	 */
    List<Integer> belongsToClass(Hand hand);
}
