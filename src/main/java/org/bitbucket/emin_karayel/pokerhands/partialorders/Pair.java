package org.bitbucket.emin_karayel.pokerhands.partialorders;

import java.util.Arrays;
import java.util.stream.Stream;

public final class Pair extends RankDistributionClass
{
    private static final Pair instance = new Pair();
    
    public static Pair getInstance()
    {
        return instance;
    }
    
    private Pair() {
        super(Arrays.asList(1,1,1,2), false);
    }
}
