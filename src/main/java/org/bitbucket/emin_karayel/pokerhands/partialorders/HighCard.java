package org.bitbucket.emin_karayel.pokerhands.partialorders;

public final class HighCard extends RankDistributionClass {
    
    private static final HighCard instance = new HighCard();
    
    public static HighCard getInstance()
    {
        return instance;
    }
    
    private HighCard() {
        super(null, false);
    }
}
