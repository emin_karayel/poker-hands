package org.bitbucket.emin_karayel.pokerhands.partialorders;

import java.util.List;

import org.bitbucket.emin_karayel.pokerhands.Hand;

public final class StraightFlush implements PokerHandClass {
    
    private StraightFlush()
    {
        
    }
    
    private static final StraightFlush instance = new StraightFlush();
    
    public static StraightFlush getInstance()
    {
        return instance;
    }
    
    @Override
    public List<Integer> belongsToClass(Hand hand) {
        if ( Flush.getInstance().belongsToClass(hand) == null ) return null;
        return Straight.getInstance().belongsToClass(hand);
    }

}
