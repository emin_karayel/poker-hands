package org.bitbucket.emin_karayel.pokerhands.partialorders;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bitbucket.emin_karayel.pokerhands.Card;
import org.bitbucket.emin_karayel.pokerhands.Hand;
import org.bitbucket.emin_karayel.pokerhands.Card.Rank;

/**
 * Generic PartialOrder for hands defined by a specific distribution of ranks
 * e.g. Two-Pair, Three-Of-Kind, but not Straight or Flush 
 */
public abstract class RankDistributionClass implements PokerHandClass {
    private List<Integer> distribution;
    private boolean compareOnlyFirst;

    public RankDistributionClass(List<Integer> distribution, boolean compareOnlyFirst)
    {
        this.distribution = distribution;
        this.compareOnlyFirst = compareOnlyFirst;
    }
    
    private HashMap<Card.Rank, Integer> getRankDistribution(Hand hand)
    {
        HashMap<Card.Rank, Integer> result = new HashMap<>();
        for (int i = 0; i < Hand.HAND_SIZE; i++) {
            Card.Rank r = hand.getCard(i).getRank();
            int prevCount = result.getOrDefault(r, 0);
            result.put(r, prevCount+1);
        }
        return result;
    }
    
    @Override
    public List<Integer> belongsToClass(Hand hand) {
        HashMap<Card.Rank, Integer> rankDistribution = getRankDistribution(hand);
        //List<Integer> c = ;
        if ( this.distribution == null || 
            rankDistribution.values().stream().sorted().collect(Collectors.toList()).equals(this.distribution) ) {
            Stream<Integer> ranks = rankDistribution.entrySet().stream().sorted((o1,o2) -> {
                int countComp = o1.getValue().compareTo(o2.getValue());
                if (countComp != 0) return -countComp;
                return -o1.getKey().compareTo(o2.getKey());
            }).map(e -> e.getKey().ordinal());
            if ( compareOnlyFirst ) {
                return ranks.limit(1).collect(Collectors.toList());
            } else {
                return ranks.collect(Collectors.toList());
            }
        } else {
            return null;
        }
    }
}
