package org.bitbucket.emin_karayel.pokerhands.partialorders;

import java.util.List;

import org.bitbucket.emin_karayel.pokerhands.Card;
import org.bitbucket.emin_karayel.pokerhands.Hand;
import org.bitbucket.emin_karayel.pokerhands.Card.Suite;

public final class Flush implements PokerHandClass 
{
    private Flush()
    {
        
    }

    private static final Flush instance = new Flush();

    public static Flush getInstance()
    {
        return instance;
    }
    
    @Override
    public List<Integer> belongsToClass(Hand hand) {
        Card.Suite suite = hand.getCard(0).getSuite();
        for(int i = 1; i < Hand.HAND_SIZE; i ++) {
            if (!hand.getCard(i).getSuite().equals(suite)) return null;
        }
        return HighCard.getInstance().belongsToClass(hand);
    }

}
