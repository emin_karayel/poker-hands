package org.bitbucket.emin_karayel.pokerhands.partialorders;

import java.util.Arrays;

public final class TwoPair extends RankDistributionClass {
    
    private static final TwoPair instance = new TwoPair();
    
    public static TwoPair getInstance()
    {
        return instance;
    }
    
    private TwoPair() {
        super(Arrays.asList(1,2,2), false);
    }
}
