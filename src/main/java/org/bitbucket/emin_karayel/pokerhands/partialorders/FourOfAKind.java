package org.bitbucket.emin_karayel.pokerhands.partialorders;

import java.util.Arrays;

public final class FourOfAKind extends RankDistributionClass 
{
    private static final FourOfAKind instance = new FourOfAKind();

    public static FourOfAKind getInstance()
    {
        return instance;
    }
    
    private FourOfAKind() {
        super(Arrays.asList(1,4), true);
    }
}
