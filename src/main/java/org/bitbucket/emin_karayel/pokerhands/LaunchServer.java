package org.bitbucket.emin_karayel.pokerhands;

import static spark.Spark.*;

public final class LaunchServer
{
    public static void main( String[] args ) throws Exception
    {
        MainSite mainSite = new MainSite("main-site.ftl");
        CompareHandsApi compareHandsApi = new CompareHandsApi();
        port(8080);
        staticFileLocation("images");
        get("/", (request,response) -> mainSite.handle(response));
        get("/api/comparehands", (request,response)-> 
            compareHandsApi.handle(request.queryMap().toMap(),response)
        ); 
    }
}

