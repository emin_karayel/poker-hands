package org.bitbucket.emin_karayel.pokerhands;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

/**
 * Renders a freemarker template
 */
public class FreeMarker {
    
    private final Configuration cfg;
 
    public FreeMarker() {
        cfg = new Configuration(Configuration.VERSION_2_3_23);
        cfg.setClassForTemplateLoading(FreeMarker.class,"/");
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
    }
       
    public String renderTemplate(String resourceName, Map<String,Object> model)
            throws IOException, TemplateException
    {
        StringWriter wr = new StringWriter();
        Template t = cfg.getTemplate(resourceName);
        t.process(model, wr);
        return wr.toString(); 
    }
}
