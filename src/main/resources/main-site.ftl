<!DOCTYPE html>
<html><body>
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script>
		var sides = {
			A: { name: "A", assignment: start() },
			B: { name: "B", assignment: start() }
		}
		
		var suites = [<#list suites as s>'${s}'<#sep>,</#list>];
		var ranks = [<#list ranks as r>'${r}'<#sep>,</#list>];
		var ranksuffixes = [<#list ranksuffixes as rs>'${rs}'<#sep>,</#list>];
		
		function start() {
			return [ <#list 0..(handsize-1) as cardindex>
				{ suite: ${cardindex} % ${suites?size}, rank: ${cardindex} }<#sep>,
			</#list> ];
		}
		
		function dec(arr, value) { return (value + arr.length - 1) % arr.length; }
		function inc(arr, value) { return (value + 1) % arr.length; }
		
		function left(side, cardindex) { updateSuite(side, cardindex, dec); }
		function right(side, cardindex) { updateSuite(side, cardindex, inc); }
		function up(side, cardindex) { updateRank(side, cardindex, inc); }
		function down(side, cardindex) { updateRank(side, cardindex, dec); }
		
		function updateSuite(side, cardindex, updatefun) {
			update(side, cardindex, function(c) {
				return { suite: updatefun(suites, c.suite), rank: c.rank };
			});
		}
		
		function updateRank(side, cardindex, updatefun) {
			update(side, cardindex, function(c) {
				return { suite: c.suite, rank: updatefun(ranks, c.rank) };
			});
		}
		
		function compressAssignment(side) {
			return side.assignment.map(function(c) { return c.rank +","+c.suite; }).join(",");
		}
		
		function update(side, cardindex, updatefun) {
			side.assignment[cardindex] = updatefun(side.assignment[cardindex]);
			updateImage(side, cardindex);
			document.getElementById("result").innerHtml = "Getting data ...";
			var text = '';
			jQuery.ajax("/api/comparehands",{
				data: {
					a: compressAssignment(sides.A),
					b: compressAssignment(sides.B)
				},
			    contentType: "application/json",
			    dataType: 'jsonp',
			    success: function(json) {
			    	switch(json.comparisonResult) {
			    		case "WIN": text = "Hand A wins!"; break;
			    		case "LOOSE": text = "Hand B wins!"; break;
			    		case "DRAW": text = "Draw!"; break; break;
			    		case "ILLEGAL": text = "One of the hands were illegal!"; break;
			    	};
			    	document.getElementById("result").innerHTML = text;
			    }
			});
		}
		
		function updateImage(side, cardindex) {
			var card = side.assignment[cardindex];
			var imgUrl = ranks[card.rank]+"_of_"+suites[card.suite]+ranksuffixes[card.rank]+".png";
			document.getElementById(side.name+""+cardindex).src = imgUrl;
		}
		
		function init(side) {
			for(var i = 0; i < ${handsize}; i++) updateImage(side, i, function(x){return x;});
		}
	</script>
    <h1>Poker Hands</h1>
    <#list ["A", "B"] as side>
        <p>Choose hand ${side}:</p>
        <table>
            <tr><#list 0..(handsize-1) as cardindex>
            	<td />
            	<td style='text-align:center'>
            		<button onclick='up(sides.${side},${cardindex})'>&#8593;</button>
            	</td>
            	<td />
            </#list></tr>
            <tr><#list 0..(handsize-1) as cardindex>
            	<td><button onclick='left(sides.${side},${cardindex})'>&#8592;</button></td>
            	<td><img id='${side}${cardindex}' src='/red_joker.png' style='width:100px' /></td>
            	<td><button onclick='right(sides.${side},${cardindex})'>&#8594;</button></td>
            </#list></tr>
            <tr><#list 0..(handsize-1) as cardindex>
            	<td />
            	<td style='text-align:center'>
            		<button onclick='down(sides.${side},${cardindex})'>&#8595;</button>
            	</td>
            	<td />
            </#list></tr>
        </table>
	</#list>
	<p>Result:</p>
	<p id='result'>Is the server running?</p>
	<script>
		init(sides.A);
		init(sides.B);	
	</script>
</body></html>
